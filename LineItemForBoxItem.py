'''
Created on 3.4.2013

@author: Klaus Nygard
'''
import logging
from PyQt4 import QtGui

class LineItemForBoxItem(QtGui.QLineEdit):
    '''
    This class extends QLineEdit and is used to monitor
    the text entry for BoxItems and to update it accordingly.
    '''
    def __init__(self, BoxItem):
        '''
        Initializes item.
        '''
        super(LineItemForBoxItem, self).__init__()
        
        # Get QGraphicsTextItem object
        self.BoxItem = BoxItem
         
        
    def updateContent(self):
        '''
        Updates the text content of a BoxItem.
        '''
        self.BoxItem.setContent(self.text())
        
        # Update box
        self.BoxItem.update()
        
        # Debug log
        logging.debug('BoxLineItem updateContent signal received')  