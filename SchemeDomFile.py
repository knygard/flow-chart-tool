'''
Created on 10.4.2013

@author: Klaus Nygard
'''

import logging
from PyQt4 import QtXml
from PyQt4 import QtCore
from PyQt4 import QtGui
from BoxItem import BoxItem
from LineItem import LineItem
from FreeDrawItem import FreeDrawItem

class SchemeDomFile(object):
    '''
    classdocs
    '''

    @staticmethod
    def save(out, items):
        '''
        Static method. Saves items to given destination as an XML-file. 
        @type out: QTextStream
        @param out: Destination stream for the file.
        @type items: list
        @param items: List of items to be saved.
        '''
        # Create dom document
        doc = QtXml.QDomDocument()
        
        # Header
        header = QtXml.QDomProcessingInstruction(doc.createProcessingInstruction("xml", "version=\"1.0\""))
        doc.appendChild(header)
        
        # Add root element to file
        rootElement = doc.createElement("scene")
        doc.appendChild(rootElement)
            
        # Add every item on scene to the file
        for item in items:
            if isinstance(item, BoxItem):
                # Create item
                itemElement = doc.createElement("BoxItem")
                rootElement.appendChild(itemElement)
                
                # Create properties
                valueElement = doc.createElement("position")
                itemElement.appendChild(valueElement)
                origin = "%s,%s" % (int(item.getOrigin().x()), int(item.getOrigin().y()))
                valueElement.appendChild(doc.createTextNode(origin))
                
                valueElement = doc.createElement("size")
                itemElement.appendChild(valueElement)
                size = "%s,%s" % (item.getWidth(), item.getHeight())
                valueElement.appendChild(doc.createTextNode(size))
                
                valueElement = doc.createElement("background")
                itemElement.appendChild(valueElement)
                bgcolor = item.getBackgroundColor()
                background = "%s,%s,%s" % (bgcolor.red(), bgcolor.green(), bgcolor.blue())
                valueElement.appendChild(doc.createTextNode(background))
                
                valueElement = doc.createElement("font-bold")
                itemElement.appendChild(valueElement)
                if item.fontIsBold():
                    fb = "yes"
                else:
                    fb = "no"
                valueElement.appendChild(doc.createTextNode(fb))
                
                valueElement = doc.createElement("font-size")
                itemElement.appendChild(valueElement)
                fontSize = str(item.getFontSize())
                valueElement.appendChild(doc.createTextNode(fontSize))
                
                valueElement = doc.createElement("text")
                itemElement.appendChild(valueElement)
                text = item.getTextItem().toPlainText()
                valueElement.appendChild(doc.createTextNode(text))
                
            elif isinstance(item, LineItem):
                
                # Create item
                itemElement = doc.createElement("LineItem")
                rootElement.appendChild(itemElement)
                
                # Create properties
                valueElement = doc.createElement("origin")
                itemElement.appendChild(valueElement)
                origin = str(item.pos().x()) + "," + str(item.pos().y())
                valueElement.appendChild(doc.createTextNode(origin))
                
                valueElement = doc.createElement("line")
                itemElement.appendChild(valueElement)
                points = str(item.line().x2()) + "," + str(item.line().y2())
                valueElement.appendChild(doc.createTextNode(points))
                
                valueElement = doc.createElement("color")
                itemElement.appendChild(valueElement)
                color = item.getColor()
                strokeColor = str(color.red()) + "," + str(color.green()) + "," + str(color.blue())
                valueElement.appendChild(doc.createTextNode(strokeColor))
                
            elif isinstance(item, FreeDrawItem):
                
                # Create item
                itemElement = doc.createElement("FreeDrawItem")
                rootElement.appendChild(itemElement)
                
                # Create properties
                valueElement = doc.createElement("origin")
                itemElement.appendChild(valueElement)
                origin = str(item.pos().x()) + "," + str(item.pos().y())
                valueElement.appendChild(doc.createTextNode(origin))
                
                valueElement = doc.createElement("points")
                itemElement.appendChild(valueElement)
                points = ""
                for i in item.childItems():
                    points += str(i.line().x1()) + "," + str(i.line().y1())
                    if not i == item.childItems()[-1]:
                        points += ";"
                valueElement.appendChild(doc.createTextNode(points))
                
                valueElement = doc.createElement("color")
                itemElement.appendChild(valueElement)
                color = item.getColor()
                strokeColor = str(color.red()) + "," + str(color.green()) + "," + str(color.blue())
                valueElement.appendChild(doc.createTextNode(strokeColor))
        
        doc.save(out, 4)
        
        
        
    ''' LOADER METHODS '''
    @staticmethod
    def loadBoxItem(domNode):
        '''
        Returns given QDomNode as BoxItem.
        @type domNode: QDomNode
        @rtype: BoxItem
        @return: BoxItem
        '''
        box = BoxItem()
        
        entry = domNode.firstChild()
        
        while not entry.isNull():
            
            data = entry.toElement()
            tag = data.tagName()
            value = data.text()
            
            if tag == "position":
                x = value.split(',')[0]
                y = value.split(',')[1]
                box.moveTo(QtCore.QPointF(int(x), int(y)))
            elif tag == "size":
                width = value.split(',')[0]
                height = value.split(',')[1]
                box.setSize(width, height)
            elif tag == "text":
                box.setText(value)
            elif tag == "background":
                rgb = value.split(',')
                color = QtGui.QColor(int(rgb[0]),int(rgb[1]),int(rgb[2]),255)
                box.setBackgroundColor(color)
            elif tag == "font-size":
                box.setFontSize(int(value))
            elif tag == "font-bold":
                if value == "yes":
                    box.toggleFontWeight()
            else:
                logging.error("Invalid tag (%s) for BoxItem found in loaded file" % tag)
                
            entry = entry.nextSibling()
        
        # Update box and add it to the scene
        box.update()
        
        logging.debug("BoxItem added to scene")
        
        return box
    
    @staticmethod
    def loadLineItem(domNode):
        '''
        Returns given QDomNode as LineItem.
        @type domNode: QDomNode
        @rtype: LineItem
        @return: LineItem
        '''
        lineItem = LineItem()
        
        entry = domNode.firstChild()
        while not entry.isNull():
            
            data = entry.toElement()
            tag = data.tagName()
            value = data.text()
            
            if tag == "origin":
                x = value.split(',')[0]
                y = value.split(',')[1]
                lineItem.setPos(QtCore.QPointF(float(x),float(y)))
            elif tag == "line":
                x = value.split(',')[0]
                y = value.split(',')[1]
                line = QtCore.QLineF(QtCore.QPointF(0,0), QtCore.QPointF(float(x), float(y)))
                lineItem.setLine(line)
            elif tag == "color":
                rgb = value.split(',')
                color = QtGui.QColor(int(rgb[0]),int(rgb[1]),int(rgb[2]),255)
                lineItem.setColor(color)
            else:
                logging.error("Invalid tag (%s) for LineItem found in loaded file" % tag)
                
            entry = entry.nextSibling()        
        
        return lineItem
    
    @staticmethod
    def loadFreeDrawItem(domNode):
        '''
        Returns given QDomNode as FreeDrawItem.
        @type domNode: QDomNode
        @rtype: FreeDrawItem
        @return: FreeDrawItem
        '''
        fdItem = FreeDrawItem()
        
        entry = domNode.firstChild()
        while not entry.isNull():
            
            data = entry.toElement()
            tag = data.tagName()
            value = data.text()
            
            if tag == "origin":
                x = value.split(',')[0]
                y = value.split(',')[1]
                point = QtCore.QPointF(float(x),float(y))
                fdItem.setPos(point)
                fdItem.setOrigin(point)
                
            elif tag == "points":
                points = value.split(';')
                prev = "0,0"
                for point in points:
                    
                    x1 = point.split(",")[0]
                    y1 = point.split(",")[1]
                    x2 = prev.split(",")[0]
                    y2 = prev.split(",")[1]
                    
                    prev = point
                    if point == points[0]:  # skip first item
                        continue
                    
                    line = QtCore.QLineF(QtCore.QPointF(float(x1),float(y1)), QtCore.QPointF(float(x2), float(y2)))
                    fdItem.addLine(line)
                    
            elif tag == "color":
                rgb = value.split(',')
                color = QtGui.QColor(int(rgb[0]),int(rgb[1]),int(rgb[2]),255)
                fdItem.setColor(color)
                
            else:
                logging.error("Invalid tag (%s) for LineItem found in loaded file" % tag)
                
            entry = entry.nextSibling()        
        
        return fdItem
    
    