'''
Created on 9.3.2013

@author: Klaus Nygard
'''

import logging
from PyQt4 import QtGui
from PyQt4.QtCore import QPointF
from BoxItem import BoxItem


class BoxToolView(QtGui.QGraphicsView):
    '''
    This class extends QGraphicsView to set the Box Tool.
    '''
    def __init__(self, undoStack):
        '''
        Initializes view with null values.
        @type undoStack: QUndoStack
        @param undoStack: UndoStack assigned to the scene.
        '''
        super(BoxToolView, self).__init__()
        
        self.undoStack = undoStack
        
        # Set initial values
        self.endpos = 0
        self.startpos = 0
        self.width = 0
        self.height = 0
        
                
    def mousePressEvent(self, event):
               
        # Set initial mouse end and start points
        self.endpos = 1
        self.startpos = QPointF(event.pos())
        
        # Create and add the temborary box      
        self.box = BoxItem()
        self.scene().addItem(self.box)
        
        # Set properties
        self.box.setBackgroundColor(QtGui.QColor(self.scene().property("selectedColor")))
        self.box.setPos(self.startpos)
        self.box.setOrigin(self.startpos)
        
        # Set helping properties for selection tool
        self.origin = self.pos()
                
        # Logging
        logging.debug('Box created by start point at: %s' % self.startpos)


    def mouseMoveEvent(self, event):
        
        if self.endpos == 1:
            
            width = self.box.mapFromScene(QPointF(event.pos())).x()
            height = self.box.mapFromScene(QPointF(event.pos())).y()
            
            if width < 0:
                width = -1*width
            if height < 0:
                height = -1*height
                
            self.width = width
            self.height = height
                               
            self.box.setSize(self.width, self.height)
        
        
    def mouseReleaseEvent(self, event):
        
        # Delete temporary box
        self.scene().removeItem(self.box)
        del self.box
        
        # Set ending point
        self.endpos = QPointF(event.pos())
        
        # Add box
        command = AddBoxItemCommand(self.scene(), self.undoStack, self.startpos, self.width, self.height, "Added BoxItem at %s" % str(self.startpos))
        self.undoStack.push(command)          
        
        # Default mouseReleaseEvent
        QtGui.QGraphicsView.mouseReleaseEvent(self, event)


class AddBoxItemCommand(QtGui.QUndoCommand):
    '''
    Command to add BoxItem to the scene. Stored in undo-stack.
    '''
    
    def __init__(self, scene, undoStack, position, width, height, description):
        '''
        Initializes command.
        @type scene: QGraphicsScene
        @type undoStack: QUndoStack
        @type position: QPointF
        @type width: float
        @type height: float
        @type description: str
        '''
        super(AddBoxItemCommand, self).__init__(description)
        
        self.scene = scene
        self.position = position
        self.height = height
        self.width = width
        self.item = 0
        self.undoStack = undoStack
        
        
    def redo(self):
        
        # Create and add the box      
        if not self.item:
            self.item = BoxItem()             
        
        # Set properties
        self.item.setBackgroundColor(QtGui.QColor(self.scene.property("selectedColor")))
        self.item.setPos(self.position)
        self.item.setOrigin(self.position)
        self.item.setSize(self.width, self.height)
        self.item.setUndoStack(self.undoStack)
        
        # Add to scene
        self.scene.addItem(self.item)
        
        logging.debug('Item added by AddBoxItemCommand.redo at position: %s' % self.position)
        

    def undo(self):
        
        self.scene.removeItem(self.item)
        
        logging.debug('Item removed by AddBoxItemCommand.undo')
    
