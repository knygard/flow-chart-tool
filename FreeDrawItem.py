'''
Created on 30.3.2013

@author: Klaus Nygard
'''

from PyQt4 import QtGui

class FreeDrawItem(QtGui.QGraphicsItemGroup):
    '''
    FreeDrawItem is used to draw figures freely on the canvas.
    '''
    def __init__(self):
        '''
        Initializes item. Sets it movable and selectable.
        '''
        super(FreeDrawItem, self).__init__()
        
        self.origin = 0
        self.pen = QtGui.QPen()
                              
        # Set flags
        self.setFlag(QtGui.QGraphicsItemGroup.ItemIsMovable)
        self.setFlag(QtGui.QGraphicsItemGroup.ItemIsSelectable)
        
    
    def addLine(self, line):
        '''
        Adds line to the item.
        @type line: QLineF
        '''
        
        item = QtGui.QGraphicsLineItem(line)
        item.setPen(self.pen)
        
        self.addToGroup(item)
        
    
    def setColor(self, color):
        '''
        Sets item color.
        @type color: QColor
        '''
        
        self.pen.setColor(color)
        for line in self.childItems():
            line.setPen(self.pen)
        
    
    def getColor(self):
        '''
        Returns item color.
        @rtype: QColor
        '''
        return self.pen.color()
    
    
    def setOrigin(self, origin):
        '''
        Sets item origin.
        @type origin: QPointF
        '''
        self.origin = origin
        
    
    def getOrigin(self):
        '''
        Returns item origin.
        @rtype: QPointF
        '''
        return self.origin
    
    
    def moveTo(self, position):
        '''
        Takes: QPointF
        Descriptions: moves item to position and saves new origin
        '''        
        # Set new position
        self.setPos(position)
        self.setOrigin(position)