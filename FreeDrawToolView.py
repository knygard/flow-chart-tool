'''
Created on 6.3.2013

@author: Klaus Nygard
'''

import logging
from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4.QtCore import QPointF
from FreeDrawItem import FreeDrawItem

class FreeDrawToolView(QtGui.QGraphicsView):
    '''
    This class needs to be inherited from the QGraphicsView
    so that we can get the mouse events for drawing.
    '''
    
    def __init__(self, undoStack):
        '''
        Initializes view with null values.
        @type undoStack: QUndoStack
        @param undoStack: UndoStack assigned to the scene.
        '''
        super(FreeDrawToolView, self).__init__()
        
        self.undoStack = undoStack
        
        self.drag = 0
        
        
    def mousePressEvent(self, event):
        '''
        Saves the starting position of the scribble.
        The event coordinates are converted to the scene coordinates
        '''
        self.startpos = QPointF(event.pos())
                
        # Create new FreeDrawItem
        self.fdItem = FreeDrawItem()
        self.fdItem.setOrigin(self.startpos)
        self.fdItem.setPos(self.startpos)
        
        # Add fdItem to scene
        self.scene().addItem(self.fdItem)
        
        # Set properties
        self.fdItem.setColor(QtGui.QColor(self.scene().property("selectedColor")))
        
        # Record position
        self.current_pos = QPointF(event.pos())
        
        # Turn dragging on
        self.drag = 1

    def mouseMoveEvent(self, event):
        '''
        Draws a line from the previous to the current.
        The new line item has to be added to the scene.
        '''
        if self.drag == 0:
            return

        # Record position
        end_pos = self.mapToScene(event.pos())
        
        # Construct line
        self.fdItem.addLine(QtCore.QLineF(self.current_pos, end_pos))
            
        # Change current position for new line
        self.current_pos = end_pos
        
        
    def mouseReleaseEvent(self, event):
        '''
        Stops dragging. Adds line by object AddFreeDrawItemCommand.
        '''
        # Turn dragging off
        self.drag = 0
        
        # Remove fdItem from scene
        self.scene().removeItem(self.fdItem)
        
        # Add line by command
        command = AddFreeDrawItemCommand(self.scene(), self.undoStack, self.fdItem, "Added LineItem")
        self.undoStack.push(command)
    
    
class AddFreeDrawItemCommand(QtGui.QUndoCommand):
    '''
    Command to add FreeDrawItem to the scene. Stored in undo-stack.
    '''
    
    def __init__(self, scene, undoStack, fdItem, description):
        '''
        Initializes command.
        @type scene: QGraphicsScene
        @type undoStack: QUndoStack
        @type fdItem: FreeDrawItem
        @type description: str
        '''
        super(AddFreeDrawItemCommand, self).__init__(description)
        
        self.scene = scene
        self.fdItem = fdItem
        self.undoStack = undoStack
        
        
    def redo(self):

        self.scene.addItem(self.fdItem)
                
        logging.debug('Line added by AddFreeDrawItemCommand.redo')
        

    def undo(self):
        
        self.scene.removeItem(self.fdItem)
        
        logging.debug('Line removed by AddFreeDrawItemCommand.undo')