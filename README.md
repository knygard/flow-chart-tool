
# Flow chart tool

Python + QT excercise: a simple flow chart drawing tool utilizing QT-library. The program was not overly complex, yet a good introduction to GUI programming. The features included drawing basic shapes, writing inside them, modifying the text, drawing simple lines, grouping and moving them etc. Also, it has an unlimited undo/redo and open/save functionalities.

## Screenshots

![](http://bytebucket.org/knygard/flow-chart-tool/raw/e2c30e312e420f7cd7a2e7d6e349b7a3e30efa70/screenshots/flow-chart-tool-1.jpg)

![](http://bytebucket.org/knygard/flow-chart-tool/raw/e2c30e312e420f7cd7a2e7d6e349b7a3e30efa70/screenshots/flow-chart-tool-2.jpg)

![](http://bytebucket.org/knygard/flow-chart-tool/raw/e2c30e312e420f7cd7a2e7d6e349b7a3e30efa70/screenshots/flow-chart-tool-3.jpg)

## License

#### Flow chart tool

[__GNU GPLv3__](http://www.gnu.org/licenses/gpl.html)

##### Icons

Icons downloaded from [Woothemes.com](http://www.woothemes.com/2009/09/woofunction-178-amazing-web-design-icons/) by [__GNU GPLv3__](http://www.gnu.org/licenses/gpl.html) license.
