'''
Created on 22.2.2013

@author: Klaus Nygard

@var WINDOW_TITLE: Title of the window.
@var APPLICATION_NAME: Name of the application.
@var DEFAULT_SELECTED_COLOR: Initially selected color.
'''
WINDOW_TITLE = "Scheme Tool"
APPLICATION_NAME = "Scheme Tool"
DEFAULT_SELECTED_COLOR = "200,210,240"

import logging
from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4 import QtXml
from SelectionToolView import SelectionToolView
from FreeDrawToolView import FreeDrawToolView
from LineToolView import LineToolView
from BoxToolView import BoxToolView
from LineItemForBoxItem import LineItemForBoxItem
from SpinBoxForBoxItem import SpinBoxForBoxItem
from BoxItem import BoxItem
from SchemeDomFile import SchemeDomFile


class MainWindow(QtGui.QMainWindow):
    '''
    This class extends QMainWindow and creates a main window with toolbars and sets the drawing 
    environment.
    '''
    
    def __init__(self):
        '''
        Sets the window parameters and calls methods
        for setting toolbars, scene and the view.
        '''
        super(MainWindow, self).__init__()
        
        # Initial filename
        self.filename = False
        
        # Set UndoStack
        self.undoStack = QtGui.QUndoStack(self)                         
        
        # Settings
        self.setWindowTitle(WINDOW_TITLE) # title
        
        # Set scene
        self.scene = QtGui.QGraphicsScene()
        self.scene.setSceneRect(QtCore.QRectF(QtCore.QPointF(0,0), QtCore.QPointF(600,480)))
        
        # Set initial selected color
        rgb = DEFAULT_SELECTED_COLOR.split(',')
        self.selectedColor = QtGui.QColor(int(rgb[0]), int(rgb[1]), int(rgb[2]), 255)
        self.scene.setProperty("selectedColor", self.selectedColor)
        
        # Set toolbars and size
        self.set_main_toolbar()
        self.set_left_toolbar()
        self.set_right_toolbar()

        self.resize(1024, 768)
        self.setMinimumSize(800, 600)
        self.updateSceneArea()
        
        # Initial tool
        self.setSelectionTool()
    
    
    def updateSceneArea(self):
        '''
        Updates scene rectangle to hide scroll bars.
        '''
        sceneWidth = self.width() - self.left_toolbar.width()
        
        if self.right_toolbar.isVisible():
            sceneWidth -= self.right_toolbar.width()
        
        sceneHeight = self.height() - self.main_toolbar.height()
    
        self.scene.setSceneRect(QtCore.QRectF(QtCore.QPointF(0,0), QtCore.QPointF(sceneWidth, sceneHeight)))
        
        logging.debug("Scene area updated.")
        

    def resizeEvent(self, event):
        '''
        Redefines resizeEvent(). Calls updateSceneArea().
        '''
        self.updateSceneArea()
        
    
    def closeEvent(self, event):
        '''
        Redefines closeEvent()
        @type event: QEvent
        '''
        if self.scene.items():
            if QtGui.QMessageBox.Cancel == QtGui.QMessageBox.warning(self, "Are you sure?", "All unsaved actions will be discarded.\nContinue?", QtGui.QMessageBox.Ok, QtGui.QMessageBox.Cancel):
                event.ignore()
                return
            
        logging.info("Program quit")


    def set_main_toolbar(self):
        '''
        Sets main toolbar.
        '''
        # Add toolbar
        self.main_toolbar = QtGui.QToolBar(self)
        self.main_toolbar.setMovable(False)

        self.addToolBar(QtCore.Qt.TopToolBarArea, self.main_toolbar)
            
        # Clear button
        clear_action = QtGui.QAction(QtGui.QIcon('icons/page_blank_32.png'), 'New', self)
        clear_action.setShortcut(QtGui.QKeySequence.New)
        clear_action.triggered.connect(self.newFile)
        self.main_toolbar.addAction(clear_action)
        
        self.main_toolbar.addSeparator()
        
        # Open file button
        open_file_action = QtGui.QAction(QtGui.QIcon('icons/folder_32.png'), 'Open', self)
        open_file_action.setShortcut(QtGui.QKeySequence.Open)
        open_file_action.triggered.connect(self.open_dialog)
        self.main_toolbar.addAction(open_file_action)
        
        # Save file button
        save_file_action = QtGui.QAction(QtGui.QIcon('icons/save_32.png'), 'Save', self)
        save_file_action.setShortcut(QtGui.QKeySequence.Save)
        save_file_action.triggered.connect(self.save_file_dialog)
        self.main_toolbar.addAction(save_file_action)
        
        self.main_toolbar.addSeparator()
        
        # Export button
        export_file_action = QtGui.QAction(QtGui.QIcon('icons/export_32.png'), 'Export as an image', self)
        export_file_action.setShortcut(QtGui.QKeySequence(QtCore.Qt.CTRL + QtCore.Qt.Key_E))
        export_file_action.triggered.connect(self.export_file_dialog)
        self.main_toolbar.addAction(export_file_action)
        
        self.main_toolbar.addSeparator()
            
        # Undo button
        undo_action = QtGui.QAction(QtGui.QIcon('icons/arrow_left_32.png'), 'Undo', self)
        undo_action.setShortcut(QtGui.QKeySequence.Undo)
        undo_action.triggered.connect(self.undoStack.undo)
        self.main_toolbar.addAction(undo_action)
        
        # Redo button
        redo_action = QtGui.QAction(QtGui.QIcon('icons/arrow_right_32.png'), 'Redo', self)
        redo_action.setShortcut(QtGui.QKeySequence.Redo)
        redo_action.triggered.connect(self.undoStack.redo)
        self.main_toolbar.addAction(redo_action)
        
        
    def set_left_toolbar(self):
        '''
        Sets left toolbar.
        '''
        # Add toolbar
        self.left_toolbar = QtGui.QToolBar(self)
        self.left_toolbar.setStyleSheet("QToolBar {background-color: red;}")
        self.left_toolbar.setAutoFillBackground(True)
        self.left_toolbar.setMovable(False)
        self.addToolBar(QtCore.Qt.LeftToolBarArea, self.left_toolbar)
        
        # Set SelectionTool
        selection_action = QtGui.QAction(QtGui.QIcon('icons/cursor_32_3.png'), 'Selection Tool (V)', self)
        selection_action.setShortcut('V')
        selection_action.triggered.connect(self.setSelectionTool)
        self.left_toolbar.addAction(selection_action)      
        self.left_toolbar.addSeparator()
        
        # Set BoxTool
        boxtool = QtGui.QAction(QtGui.QIcon('icons/element_32.png'), 'Element Tool (E)', self)
        boxtool.setShortcut('E')
        boxtool.triggered.connect(self.setBoxTool)
        self.left_toolbar.addAction(boxtool)
        self.left_toolbar.addSeparator()
        
        # Set LineTool
        linetool = QtGui.QAction(QtGui.QIcon('icons/linetool_32.png'), 'Line Tool (L)', self)
        linetool.setShortcut('L')
        linetool.triggered.connect(self.setLineTool)
        self.left_toolbar.addAction(linetool)
        self.left_toolbar.addSeparator()  
        
        # Set FreeDrawTool
        freedrawtool = QtGui.QAction(QtGui.QIcon('icons/pencil_32.png'), 'Drawing Tool (P)', self)
        freedrawtool.setShortcut('P')
        freedrawtool.triggered.connect(self.setFreeDrawTool)
        self.left_toolbar.addAction(freedrawtool)
        self.left_toolbar.addSeparator()
                
        # Set color selector
        self.ColorSelector = QtGui.QPushButton()
        self.setColorBoxStyle()
        self.ColorSelector.clicked.connect(self.color_select_dialog)

        self.left_toolbar.addWidget(self.ColorSelector)
        
    
    def set_right_toolbar(self):
        '''
        Sets right toolbar.
        '''
        # Add toolbar    
        self.right_toolbar = QtGui.QToolBar(self)
        self.right_toolbar.setMovable(False)
        self.addToolBar(QtCore.Qt.RightToolBarArea, self.right_toolbar)
        self.right_toolbar.hide()
        
    
    def update_right_toolbar(self):
        '''
        Updates right toolbar based on selection.
        '''
        self.right_toolbar.clear()
        if self.scene.selectedItems():
            for item in self.scene.selectedItems():
                if not isinstance(item, BoxItem):
                    break
                self.itemPropertiesPanel(item)
                self.right_toolbar.show()
        else:
            self.right_toolbar.hide()
            
        self.updateSceneArea()
            
            
    def mouseReleaseEvent(self, event):
        '''
        Redefines mouseReleaseEvent() to update right toolbar.
        @type event: QEvent
        '''
        self.update_right_toolbar()
    
    
        
    
    
    ''' FILEHANDLING METHODS '''
        
    def save_file_dialog(self):
        '''
        Opens save file dialog and calls saveFile().
        '''
        if not self.filename:
            self.filename = QtGui.QFileDialog.getSaveFileName(self, 'Save File', 'scheme', "Scheme (*.sch)")
            
        if not self.filename:
            return False
        
        logging.info("Filename for saving: %s" % self.filename)
        
        self.saveFile(self.filename)
    
    
    def export_file_dialog(self):
        '''
        Opens save file dialog and saves scene as an image file.
        '''
        filename = QtGui.QFileDialog.getSaveFileName(self, 'Save File', 'scheme', "Images (*.png *.xpm *.jpg)")
        
        if not filename:
            return False

        # Add temporarily a white background to the scene
        bgbox = QtGui.QGraphicsRectItem(self.scene.sceneRect())
        bgbox.setBrush(QtGui.QColor(255,255,255,255))
        bgbox.setPen(QtGui.QPen(QtGui.QColor(255,255,255,255)))
        self.scene.addItem(bgbox)
        bgbox.setZValue(-1)
        
        # Create image
        img = QtGui.QImage(self.scene.sceneRect().size().toSize(), QtGui.QImage.Format_RGB32)
        painter = QtGui.QPainter(img)
        self.scene.render(painter)
        img.save(filename)
        
        # Deleter temporary items
        del painter
        self.scene.removeItem(bgbox)
        del bgbox
        
        logging.info("File exported by name: %s" % filename)
        
    
    def open_dialog(self):
        '''
        Gets path to selected file and passes it to loadFile method.
        ''' 
        if self.scene.items():
            if QtGui.QMessageBox.Cancel == QtGui.QMessageBox.warning(self, "Are you sure?", "Opening new file.\nThe current document will be discarded.", QtGui.QMessageBox.Ok, QtGui.QMessageBox.Cancel):
                return False
        
        self.filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', '.')
        if not self.filename:
            return False
                
        self.file = QtCore.QFile(self.filename)
        if not self.file.open(QtCore.QIODevice.ReadOnly or QtCore.QIODevice.Text):
            # Show error message
            QtGui.QMessageBox.critical(self, "Opening file", "Couldnt open %s" % self.file, QtGui.QMessageBox.Ok)
            return
        
        logging.info("Selected file: %s" % self.filename)
        
        self.loadFile(self.filename)
        
    
    def saveFile(self, filename):
        '''
        Saves file as XML by given filename.
        @rtype: bool
        @return: The boolean value: True if file was saved succesfully.
        '''
        # Open file to write
        qfile = QtCore.QFile(filename)
        if not qfile.open(QtCore.QIODevice.WriteOnly | QtCore.QFile.Text):
            QtGui.QMessageBox.warning(self, self.tr("SDI"),
                       self.tr("Cannot write file %1:\n%2.").arg(self.filename).arg(qfile.errorString()))
            logging.error("Cannot write to file")
            return False
        
        # Save file
        out = QtCore.QTextStream(qfile)
        QtGui.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        SchemeDomFile().save(out, self.scene.items())
        QtGui.QApplication.restoreOverrideCursor()
        
        qfile.close()    
        logging.info("File saved")
        
        return True
    
    
    def loadFile(self, filename):
        '''
        Loads XML-file by given filename to scene.
        @type filename: str
        @param filename: Path to file.
        '''
        qfile = QtCore.QFile(filename)
        
        # read file contents into an XML document
        doc = QtXml.QDomDocument("Scheme Document")
        doc.setContent(qfile)
        
        # First element (should be <scene>)
        docElem = doc.documentElement()
        logging.debug("First element loaded from file: %s" % docElem.tagName())
        
        # Check validity of the file
        if docElem.tagName() != "scene":
            QtGui.QMessageBox.critical(self, "File error", "Opened file was not valid format")
            return
        
        # Clear old items
        self.scene.clear()
        self.undoStack.clear()
        
        # Load items to scene        
        nodelist = docElem.elementsByTagName("BoxItem")
        for i in range(0, nodelist.length()):
            element = nodelist.item(i)
            item = SchemeDomFile().loadBoxItem(element)
            self.scene.addItem(item)
        logging.info("Loaded %d BoxItems" % nodelist.length())
        
        nodelist = docElem.elementsByTagName("LineItem")
        for i in range(0, nodelist.length()):
            element = nodelist.item(i)
            item = SchemeDomFile().loadLineItem(element)
            self.scene.addItem(item)
        logging.info("Loaded %d LineItems" % nodelist.length())
        
        nodelist = docElem.elementsByTagName("FreeDrawItem")
        for i in range(0, nodelist.length()):
            element = nodelist.item(i)
            item = SchemeDomFile().loadFreeDrawItem(element)
            self.scene.addItem(item)
        logging.info("Loaded %d FreeDrawItems" % nodelist.length())
        
        self.view.update()
        
            
        
    def newFile(self):
        '''
        Removes all items from the scene and flushes undoStack.
        '''
        if self.scene.items():
            if QtGui.QMessageBox.Cancel == QtGui.QMessageBox.warning(self, "Are you sure?", "Creating new file.\nThe current document will be discarded.", QtGui.QMessageBox.Ok, QtGui.QMessageBox.Cancel):
                return False
        
        self.filename = 0
        self.scene.clear()
        self.undoStack.clear()
        self.view.update()
        
    

    
    
    
    ''' INTERFACE METHODS '''
    
    def color_select_dialog(self):
        '''
        Opens color selector dialog.
        '''
        newColor = QtGui.QColorDialog.getColor()
        if newColor and newColor.isValid():
            self.selectedColor = newColor
            self.scene.setProperty("selectedColor", newColor)
        
        logging.debug("Selected color: %d, %d, %d, %d" % QtGui.QColor(self.scene.property("selectedColor")).getRgb())
        self.setColorBoxStyle()
        
           
    def reset_view(self):
        '''
        Resets view.
        '''
        self.scene.clearSelection()
        self.right_toolbar.hide()
        self.updateSceneArea()
    
    
    def set_view(self, View):
        '''
        Sets given view.
        @type View: QGraphicsView
        @param View: The view to be set active.
        '''
        # Set view        
        self.view = View
        self.view.setScene(self.scene)
        
        # Set antialiasing
        self.view.setRenderHints(QtGui.QPainter.Antialiasing | QtGui.QPainter.SmoothPixmapTransform)

        # Set view as CentralWidget
        self.setCentralWidget(self.view)
                
                
    def setColorBoxStyle(self):
        '''
        Sets color selection box style.
        '''
        self.ColorSelector.setStyleSheet("QWidget { height: 26px; margin: 7px; \
            background-color: rgb(%d, %d, %d, %d) }" % QtGui.QColor(self.scene.property("selectedColor")).getRgb())
    
    
    def closeWindow(self, event):
        '''
        Close window dialog.
        '''
        quit_msg = "Are you sure you want to exit the program?"
        reply = QtGui.QMessageBox.question(self, 'Message', 
                         quit_msg, QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
    
        if reply == QtGui.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()
            
    def itemPropertiesPanel(self, item):
        '''
        Generates panel of selected item properties.
        @type item: QGraphicsItem
        @param item: The item of which parameters will be listed on properties panel.
        '''
        # Set main group
        propertiesGroup = QtGui.QGroupBox("Item properties")
        self.right_toolbar.addWidget(propertiesGroup)
        
        # Set layout
        self.vbox = QtGui.QVBoxLayout()
        self.vbox.setSpacing(7)
        propertiesGroup.setLayout(self.vbox)
        
        # Create separator line
        separator = QtGui.QFrame()
        separator.setFrameShape(QtGui.QFrame.HLine)
        separator.setFrameShadow(QtGui.QFrame.Sunken)
            
        # Set box text content field               
        label1 = QtGui.QLabel("Box content:")
        self.vbox.addWidget(label1)
        content = LineItemForBoxItem(item)
        content.setText(QtCore.QString(item.getContent()))
        content.textEdited.connect(content.updateContent)
        self.vbox.addWidget(content)
        self.vbox.addWidget(separator)
        
        # Set font options        
        fontOption2 = SpinBoxForBoxItem(item)
        fontOption2.setRange(10, 32)
        fontOption2.setValue(item.getFontSize())
        fontOption2.valueChanged.connect(fontOption2.updateFontSize)
        spinBoxLabel = QtGui.QLabel("Font size:")
        self.vbox.addWidget(spinBoxLabel)
        self.vbox.addWidget(fontOption2)

        fontPropertiesLabel = QtGui.QLabel("Font properties:")
        self.vbox.addWidget(fontPropertiesLabel)
        
        fontOption1 = QtGui.QCheckBox("bold")
        if item.font.bold():
            fontOption1.setCheckState(2)
        fontOption1.stateChanged.connect(item.toggleFontWeight)
        self.vbox.addWidget(fontOption1)
        
        # Set color selector
        label2 = QtGui.QLabel("BG Color:")
        self.vbox.addWidget(label2)
        ColorSelector = QtGui.QPushButton()
        ColorSelector.setStyleSheet("QWidget { height: 20px; margin: 3px; \
            background-color: rgb(%d, %d, %d, %d) }" % item.getBackgroundColor().getRgb())
        ColorSelector.clicked.connect(item.change_item_bgColor)
        ColorSelector.clicked.connect(self.update_right_toolbar)        
                
        self.vbox.addWidget(ColorSelector)
    
    
    
    
    
    
    
    ''' SET TOOL - METHODS'''
    
    def setSelectionTool(self):
        '''
        Sets Selection Tool active.
        '''
        self.set_view(SelectionToolView(self.undoStack))
        
        
    def setFreeDrawTool(self):
        '''
        Sets Free Draw Tool active.
        '''
        self.reset_view()
        self.set_view(FreeDrawToolView(self.undoStack))
        

    def setLineTool(self):
        '''
        Sets Line Tool active.
        '''
        self.reset_view()
        self.set_view(LineToolView(self.undoStack))
        
        
    def setBoxTool(self):
        '''
        Sets Box Tool active.
        '''
        self.reset_view()
        self.set_view(BoxToolView(self.undoStack))
    

    