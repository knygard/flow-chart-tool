'''
Created on 3.4.2013

@author: Klaus Nygard
'''

import logging
from PyQt4 import QtGui

class SpinBoxForBoxItem(QtGui.QSpinBox):
    '''
    This class extends QSpinBox and is used to monitor
    the font size of BoxItems and update it accordingly.
    '''
    def __init__(self, BoxItem):
        '''
        Initializes item.
        '''
        super(SpinBoxForBoxItem, self).__init__()
        
        self.BoxItem = BoxItem
        self.setSuffix(" px")
        self.setObjectName("FontSize")
        
                
    def updateFontSize(self):
        '''
        Updates the font size of the BoxItem.
        '''
        self.BoxItem.setFontSize(self.value())
        
        # Debug log
        logging.debug('BoxSpinBox updateValue signal received')  