'''
Created on 20.2.2013

@author: Klaus Nygard

@var DEBUG_MODE: Set true to enable debug mode.
'''

DEBUG_MODE = True

import sys
import logging
from PyQt4 import QtGui
from MainWindow import MainWindow        

if __name__ == "__main__":
    '''
    This is a Scheme Tool application programmed on
    course T-106.1215 at Aalto University.
    '''
    # Set run mode
    if DEBUG_MODE:
        logging.basicConfig(level=logging.DEBUG)
    
    # Create QT application
    app = QtGui.QApplication(sys.argv)
    
    # Initialize main window
    window = MainWindow()
    window.show()

    exit_code = app.exec_()
    sys.exit(exit_code)