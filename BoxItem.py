'''
Created on 30.3.2013

@author: Klaus Nygard

@var BOXITEM_MIN_WIDTH: minimum width for BoxItem
@var BOXITEM_MIN_HEIGHT: minimum height for BoxItem
@var BOXITEM_DEFAULT_TEXT: default text (content) for BoxItem
'''

BOXITEM_MIN_WIDTH = 35
BOXITEM_MIN_HEIGHT = 15
BOXITEM_DEFAULT_TEXT = "Element"

import logging
from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4.QtCore import QPointF

class BoxItem(QtGui.QGraphicsRectItem):
    '''
    BoxItem is the basic box used in the Scheme Tool. It is
    a rectangle that contains size, color, text and font information.
    '''
    def __init__(self):
        '''
        Initializes item. Sets it movable and selectable.
        '''
        super(BoxItem, self).__init__()
        
        self.undoStack = 0
              
        # Set flags
        self.setFlag(QtGui.QGraphicsItem.ItemIsMovable)
        self.setFlag(QtGui.QGraphicsItem.ItemIsSelectable)
               
        # Set box properties
        self.setSize(35, 10)
        
        # Set text
        self.font = QtGui.QFont()
        self.font.setBold(False)
        self.font.setPixelSize(13)
        self.font.setFamily("Arial")
        
        self.text = QtGui.QGraphicsTextItem(QtCore.QString(BOXITEM_DEFAULT_TEXT))
        
        self.text.setParentItem(self)
        self.text.setFont(self.font)
        self.setTextPos()
        
        # Initial position
        self.moveTo(QtCore.QPointF(0,0))
        
    

    def setSize(self, width, height):
        '''
        Sets BoxItem size by width and height.
        @type width: float
        @type height: float
        '''
        self.width = int(width)
        if self.width < BOXITEM_MIN_WIDTH:
            self.width = BOXITEM_MIN_WIDTH
        
        self.height = int(height)
        if self.height < BOXITEM_MIN_HEIGHT:
            self.height = BOXITEM_MIN_HEIGHT
        
        point1 = QPointF(-self.width, -self.height)
        point2 = QPointF(self.width, self.height)
        
        self.setRect(QtCore.QRectF(point1, point2))
        
    def getWidth(self):
        '''
        @rtype: float
        '''
        return self.width
    
    
    def getHeight(self):
        '''
        @rtype: float
        '''
        return self.height
    
    
    def moveTo(self, position):
        '''
        Moves item to position and saves history value.
        @type position: QPointF
        '''        
        # Set new position
        self.setPos(position)
        self.setOrigin(position)
        
        # Update item
        self.update()
            

    def setOrigin(self, origin):
        '''
        @type origin: QPointF
        '''
        self.origin = origin
    
    
    def getOrigin(self):
        '''
        @rtype: QPointF
        '''
        if self.origin:
            return self.origin
        else:
            logging.debug('Error: no origin to return for BoxItem')
            return False


    def setContent(self, string):
        '''
        @type string: str
        '''
        self.text.setPlainText(string)


    def setBackgroundColor(self, color):
        '''
        @type color: QColor
        '''
        self.setBrush(color)
        self.setPen(color.darker(150))
    
    
    def getBackgroundColor(self):
        '''
        @rtype: QColor
        '''
        return self.brush().color()
    

    def getContent(self):
        '''
        @rtype: str
        '''
        return self.text.toPlainText()


    def getTextItem(self):
        '''
        @rtype: QGraphicsTextItem
        '''
        return self.text
    
    def setText(self, text):
        '''
        Sets the text content of a BoxItem.
        @type text: str
        @param text: BoxItem content as string.
        '''
        self.text.setPlainText(text)
    

    def setTextPos(self):
        '''
        Calculates and sets text position inside the BoxItem.
        '''     
        x = - self.text.boundingRect().width() / 2
        y = - self.text.boundingRect().height() / 2
        newPos = QPointF(x,y)
        
        self.text.setPos(newPos)
        
        
    def update(self):
        '''
        Updates BoxItem. Needed in special circumstances.
        '''
        self.text.setFont(self.font)
        self.setTextPos()
        
    
    def toggleFontWeight(self):
        '''
        Toggles font weight of the BoxItem.
        '''
        if self.fontIsBold():
            self.font.setBold(False)
            self.update()
            logging.debug('Font weight set normal')
        else:
            self.font.setBold(True)
            self.update()
            logging.debug('Font weight set bold')
            
    def fontIsBold(self):
        '''
        Returns True if font is set as bold, False otherwise.
        @rtype: bool
        @return: True/False if font is bold.
        '''
        if self.font.bold():
            return True
        else:
            return False
        
            
    def setFontFamily(self, family):
        '''
        Sets BoxItem font family.
        @type family: QString
        @param family: Takes font family name as a QString. E.g. "Arial".
        '''
        self.font.setFamily(family)
        self.update()
        
        
    def getFontFamily(self):
        '''
        @rtype: QString
        @return: Returns font family in QString.
        '''
        return self.font.family()
        
        
    def setFontSize(self, size):
        '''
        Sets BoxItem font size in pixels by integer value.
        @type size: int
        '''
        self.font.setPixelSize(size)
        self.update()
        
        
    def getFontSize(self):
        '''
        @rtype: int
        @return: Font size in pixels.
        '''
        return self.font.pixelSize()
    
    
    def setUndoStack(self, undoStack):
        '''
        Description: links a correct undoStack to the item
        '''
        self.undoStack = undoStack
    
    
    def change_item_bgColor(self):
        '''
        Description: opens color selector dialog and saves the
        selected color as new background color for the selected item.
        '''
        newColor = QtGui.QColorDialog.getColor()
        if newColor.isValid():
            self.selectedColor = newColor
            
            command = ChangeItemBGColor(self, newColor, "Change item background")
            self.undoStack.push(command)

    
    
class ChangeItemBGColor(QtGui.QUndoCommand):
    '''
    Command used to change BoxItem background color.
    This command is used in undo-stack.
    '''
    
    def __init__(self, item, color, description):
        '''
        Initializes the command to change item background color.
        This is used only for BoxItem in this version.
        @type item: QGraphicsItem
        @type color: QColor
        @type description: description
        '''
        super(ChangeItemBGColor, self).__init__(description)
        
        self.item = item
        self.color = color
        self.oldColor = item.getBackgroundColor()
        
        
    def redo(self):
        
        self.item.setBackgroundColor(self.color)

        logging.debug('Item background color changed to: %s' % self.color)
        

    def undo(self):
        
        self.item.setBackgroundColor(self.oldColor)
        
        logging.debug('Item background color changed back to: %s' % self.oldColor)
        
        
