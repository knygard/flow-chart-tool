'''
Created on 6.3.2013

@author: Klaus Nygard
'''

import logging
from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4.QtCore import QPointF

class SelectionToolView(QtGui.QGraphicsView):
    '''
    This class needs to be inherited from the QGraphicsView
    so that we can get the mouse events for selection tool.
    '''
    def __init__(self, undoStack):
        '''
        Initializes view with null values.
        @type undoStack: QUndoStack
        @param undoStack: UndoStack assigned to the scene.
        '''
        super(SelectionToolView, self).__init__()
        
        self.undoStack = undoStack
        
        # Initialize drag mode
        self.drag = False
        
        # Create rubberband
        self.rubberband = QtGui.QRubberBand(QtGui.QRubberBand.Rectangle, self)
        
        self.setMouseTracking(True)


    def mousePressEvent(self, event):
        
        self.origin = event.pos()
        
        logging.debug('Items at clicked point: %s' % self.scene().itemAt(QtCore.QPointF(event.pos())))
        
        # Dragging
        if self.scene().itemAt(QPointF(event.pos())) in self.scene().selectedItems():
            self.drag = True
            self.dragitem = self.scene().itemAt(QPointF(event.pos()))
            logging.debug('Starting drag...')
        
        # Rubberband
        else:
            self.rubberband.setGeometry(QtCore.QRect(self.origin, QtCore.QSize(1,1)))
            self.rubberband.show()
        
        
        QtGui.QWidget.mousePressEvent(self, event)


    def mouseMoveEvent(self, event):
        
        # Set (new) rubberband geometry
        if self.rubberband.isVisible():
            self.rubberband.setGeometry(QtCore.QRect(self.origin, event.pos()).normalized())
            
        # Dragging
        if self.drag:            
            # Move all items
            for item in self.scene().selectedItems():    
                # Calculate moved distance and new position for the item 
                movedDistance = QPointF(event.pos()) - QPointF(self.origin)    
                newPos = item.getOrigin() + movedDistance
                item.setPos(newPos)

            
        QtGui.QWidget.mouseMoveEvent(self, event)
        

    def mouseReleaseEvent(self, event):
        
        if self.drag:
            itemCount = 0
            # Set new origin, final position and undoStack
            for item in self.scene().selectedItems():
                newPos = QPointF(item.pos())
                
                command = MoveItemCommand(item, newPos, "Moved item to %s" % str(newPos))
                self.undoStack.push(command)
                
                itemCount += 1

            # Stop dragging
            self.drag = False
        
        # Set scene.selectionArea
        if self.rubberband.isVisible():
            
            # Hide rubberband
            self.rubberband.hide()
                        
            selectionRectangle = QtCore.QRectF(self.rubberband.geometry())
            
            command = SetSelectionCommand(self.scene(), selectionRectangle, "Selection area set")
            command.redo()
                        
        QtGui.QWidget.mouseReleaseEvent(self, event)
        
        
    def keyPressEvent(self, event):
        if event.key() == QtGui.QKeySequence.Delete or event.key() == 16777223: # delete-key on mac???
            command = DeleteItemsCommand(self.scene(), "Deleted selected items")
            self.undoStack.push(command)



class MoveItemCommand(QtGui.QUndoCommand):
    '''
    Command to move item. Stored in undo-stack.
    '''
    
    def __init__(self, item, position, description):
        '''
        Initializes command.
        @type item: QGraphicsItem
        @type position: QPointF
        @type description: str
        '''
        super(MoveItemCommand, self).__init__(description)
        
        self.item = item
        self.position = position
        
        
    def redo(self):
        
        self.oldPos = self.item.getOrigin()
            
        # Set new position for the item
        self.item.moveTo(self.position)
        
        logging.debug('Item moved by redo to position: %s' % self.position)
        

    def undo(self):
               
        # Set new position for the item
        self.item.moveTo(self.oldPos)
        
        logging.debug('Item moved by undo to position: %s' % self.oldPos)



class MoveManyItemsCommand(QtGui.QUndoCommand):
    '''
    Command to move many items at once. Stored in undo-stack.
    '''
    def __init__(self, amount, undoStack, description):
        '''
        Initializes command.
        '''
        super(MoveManyItemsCommand, self).__init__(description)
        
        self.amount = amount
        self.undoStack = undoStack
        
        
    def redo(self):
        
        i = 0
        while (i < self.amount):
            print "redo"
            self.undoStack.redo()
            i += 1
                    
                    
    def undo(self):
        
        print self.amount
        i = 0
        while (i < self.amount):
            print "undo"
            self.undoStack.undo()
            i += 1



class DeleteItemsCommand(QtGui.QUndoCommand):
    '''
    Command to delete selected items from the given scene.
    Stored in undo-stack.
    '''
    def __init__(self, scene, description):
        '''
        Initializes command.
        @type scene: QGraphicsScene
        @type description: str
        '''
        super(DeleteItemsCommand, self).__init__(description)
        
        self.scene = scene
        self.items = scene.selectedItems()
       
       
    def redo(self):
       
        for item in self.items:
            self.scene.removeItem(item)
            
        logging.debug('Deleted selected items')
    
    
    def undo(self):
    
        for item in self.items:
            self.scene.addItem(item)



class SetSelectionCommand(QtGui.QUndoCommand):
    '''
    Command to set selection. Stored in undo-stack.
    '''
    
    def __init__(self, scene, area, description):
        '''
        Initializes command.
        @type scene: QGraphicsScene
        @type area: QRectF
        @type description: str
        '''
        super(SetSelectionCommand, self).__init__(description)
        
        self.scene = scene
        self.area = area
        
        
    def redo(self):
        
        self.scene.clearSelection()
        
        selectionArea = QtGui.QPainterPath()
        selectionArea.addRect(self.area)
        self.scene.setSelectionArea(selectionArea, QtCore.Qt.IntersectsItemShape)
        
        logging.debug('Selected items: %s' % self.scene.selectedItems())

        
        
    