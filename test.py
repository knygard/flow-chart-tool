'''
Created on 30.3.2013

@author: Klaus Nygard
'''
import sys
import unittest
from PyQt4 import QtGui
from PyQt4 import QtCore
from BoxItem import BoxItem
from LineItem import LineItem
from FreeDrawItem import FreeDrawItem

class Test(unittest.TestCase):

    def setUp(self):
        '''
        Initializes QApplication
        '''
        self.app = QtGui.QApplication(sys.argv)
        self.scene = QtGui.QGraphicsScene()
        

    def test_BoxItem(self):
        
        box = BoxItem()
        self.scene.addItem(box)
        
        # Moving
        box.moveTo(QtCore.QPointF(56,13))
        box.moveTo(QtCore.QPointF(15,15))
        self.assertEqual("%s,%s" % (box.pos().x(), box.pos().y()), "15,15",\
                         "BoxItem was not moved correctly to (15,15)")
        
        # Setting size
        box.setSize(98, 34)
        self.assertEqual(box.getWidth(), 98, "BoxItem width was not set correctly")
        self.assertEqual(box.getHeight(), 34, "BoxItem height was not set correctly")
        
        # Background color
        box.setBackgroundColor(QtGui.QColor(192,111,232,255))
        bgc = box.getBackgroundColor()
        self.assertEqual("%s,%s,%s" % (bgc.red(), bgc.green() , bgc.blue()), "192,111,232",\
                         "BoxItem background-color was not set correctly")
        
    def test_LineItem(self):
        
        lineItem = LineItem()
        
        # Moving
        lineItem.moveTo(QtCore.QPointF(34,56))
        lineItem.moveTo(QtCore.QPointF(14,35))
        self.assertEqual("%s,%s" % (lineItem.pos().x(), lineItem.pos().y()),\
                         "14.0,35.0", "BoxItem was not moved correctly to (14,35)")
    
        # Line color
        lineItem.setColor(QtGui.QColor(123,134,244,255))
        color = lineItem.getColor()
        self.assertEqual("%s,%s,%s" % (color.red(), color.green() , color.blue()), "123,134,244",\
                         "LineItem color was not set correctly")
    
    
    def test_FreeDrawItem(self):
        
        fdItem = FreeDrawItem()
        
        # Moving
        fdItem.moveTo(QtCore.QPointF(34,56))
        fdItem.moveTo(QtCore.QPointF(24,123))
        self.assertEqual("%s,%s" % (fdItem.pos().x(), fdItem.pos().y()),\
                         "24.0,123.0", "BoxItem was not moved correctly to (24,123)")
        
        # adding lines
        fdItem.addLine(QtCore.QLineF(0,0,10,10))
        fdItem.addLine(QtCore.QLineF(24,35,60,128))
        fdItem.addLine(QtCore.QLineF(124,35,60,128))
        self.assertEqual(len(fdItem.childItems()), 3, "There was an error in adding line to FreeDrawItem")
        
    
 
    def exit(self):
        '''
        Exits QApplication
        '''
        try:
            exit_code = self.app.exec_()
            sys.exit(exit_code)
        except IOError:
            ''' ignore'''


if __name__ == "__main__":
    unittest.main()