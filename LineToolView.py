'''
Created on 6.3.2013

@author: Klaus Nygard
'''

import logging
from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4.QtCore import QPointF
from LineItem import LineItem

class LineToolView(QtGui.QGraphicsView):
    '''
    This class needs to be inherited from the QGraphicsView
    so that we can get the mouse events for drawing.
    '''
    
    def __init__(self, undoStack):
        '''
        Initializes view with null values.
        @type undoStack: QUndoStack
        @param undoStack: UndoStack assigned to the scene.
        '''
        super(LineToolView, self).__init__()
        
        self.undoStack = undoStack
        
        self.drag = 0
        
        
    def mousePressEvent(self, event):
        '''
        Saves the starting position of the line.
        The event coordinates are converted to the scene coordinates
        '''
        self.startpos = self.mapToScene(event.pos())
                
        # Create new LineItem
        self.lineItem = LineItem()
        self.lineItem.setOrigin(self.startpos)
        self.lineItem.setPos(self.startpos)
        self.lineItem.setLine(QtCore.QLineF(QPointF(0,0), QPointF(0,0)))
        
        # Add LineItem to scene
        self.scene().addItem(self.lineItem)
        
        # Set properties
        self.lineItem.setColor(QtGui.QColor(self.scene().property("selectedColor")))
        
        # Turn dragging on
        self.drag = 1
        
        # Logging
        logging.debug('Line created by start point at: %s' % self.startpos)


    def mouseMoveEvent(self, event):
        '''
        Draws a line from the previous to the current.
        The new line item has to be added to the scene.
        '''
        if self.drag == 0:
            return

        # Record position
        end_pos = self.mapToScene(event.pos())
        
        # Construct line
        self.lineItem.setLine(QtCore.QLineF(QPointF(0,0), end_pos - self.startpos))
        
        
    def mouseReleaseEvent(self, event):
        
        # Turn dragging off
        self.drag = 0
        
        # Remove LineItem from scene
        self.scene().removeItem(self.lineItem)
        
        # Add line by command
        command = AddLineItemCommand(self.scene(), self.undoStack, self.lineItem, "Added LineItem")
        self.undoStack.push(command)
    
    
class AddLineItemCommand(QtGui.QUndoCommand):
    '''
    Command to add LineItem to the scene. Stored in undo-stack.
    '''
    
    def __init__(self, scene, undoStack, LineItem, description):
        '''
        Initializes command.
        @type scene: QGraphicsScene
        @type undoStack: QUndoStack
        @type LineItem: LineItem
        @type description: str
        '''
        super(AddLineItemCommand, self).__init__(description)
        
        self.scene = scene
        self.lineItem = LineItem
        self.undoStack = undoStack
        
        
    def redo(self):

        self.scene.addItem(self.lineItem)
                
        logging.debug('Line added by AddLineItemCommand.redo')
        

    def undo(self):
        
        self.scene.removeItem(self.lineItem)
        
        logging.debug('Line removed by AddLineItemCommand.undo')
        
        