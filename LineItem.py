'''
Created on 30.3.2013

@author: Klaus Nygard
'''

from PyQt4 import QtGui

class LineItem(QtGui.QGraphicsLineItem):
    '''
    LineItem is used to create straight lines between two points.
    '''    
    def __init__(self):
        '''
        Initializes item. Sets it movable and selectable.
        '''
        super(LineItem, self).__init__()
        
        self.origin = 0
        self.pen = QtGui.QPen()
        self.setPen(self.pen)
        
        # Set flags
        self.setFlag(QtGui.QGraphicsLineItem.ItemIsSelectable)
        self.setFlag(QtGui.QGraphicsItem.ItemIsMovable)
    

    def setColor(self, color):
        '''
        Sets color for the LineItem.
        @type color: QColor
        '''
        self.pen.setColor(color)
        self.setPen(self.pen)
        
    
    def getColor(self):
        '''
        Returns the color of the LineItem.
        @rtype: QColor
        '''
        return self.pen.color()
        

    def setOrigin(self, origin):
        '''
        Sets origin position for the LineItem.
        @type origin: QPointF
        '''
        self.origin = origin
        
    
    def getOrigin(self):
        '''
        Returns origin position of the LineItem.
        @rtype: QPointF
        '''
        return self.origin


    def moveTo(self, position):
        '''
        Moves LineItem to position and saves new origin.
        @type position: QPointF
        '''        
        # Set new position
        self.setPos(position)
        self.setOrigin(position)
        
        